import { colors } from '..'

export const syntax = {
  borderRadius: 3,
  fontSize: '14px',
  backgroundColor: colors.white[3]
}
