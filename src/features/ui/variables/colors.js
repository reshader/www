import reshader from 'reshader'

export const colors = {
  white: reshader('#CCCCCC').palette,
  gray: reshader('#666666').palette,
  cyan: reshader('#6ed0eb', { contrast: 0.05 }).palette,
  blue: reshader('#007DFF').palette,
  acqua: reshader('#6EEB83', { contrast: 0.05 }).palette,
  yellow: reshader('#fff146').palette
}
