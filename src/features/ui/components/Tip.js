import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { colors } from '..'

const Container = styled.div`
  margin: 20px 0;
  padding: 20px;
  background-color: ${colors.cyan[1]};
  border: 1px solid ${colors.cyan[4]};
  border-radius: 3px;
`
const Label = styled.p`
  margin: 0 0 10px;
  padding: 0;
  color: ${colors.cyan[8]};
  font-weight: bold;
`
const Text = styled.p`
  margin: 0;
  padding: 0;
  line-height: initial;
  color: ${colors.gray[8]};
`

export const Tip = ({ title, children }) => (
  <Container>
    <Label>{title || 'Important to know'}</Label>

    <Text>{children}</Text>
  </Container>
)

Tip.propTypes = {
  title: PropTypes.string,
  children: PropTypes.any
}
