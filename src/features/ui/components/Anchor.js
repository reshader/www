import styled from 'styled-components'

import { colors } from '..'

export const Anchor = styled.a`
  color: ${colors.acqua[9]};

  &:hover {
    color: ${colors.acqua[10]};
  }
`
