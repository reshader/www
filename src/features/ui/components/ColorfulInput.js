import styled from 'styled-components'
import reshader from 'reshader'
import Color from 'color'

import { colors } from '..'

export const ColorfulInput = styled.input`
  display: inline-block;
  padding: 10px;
  width: auto;
  background-color: ${props =>
    props.isColorValid ? reshader(props.bg).palette[1] : colors.white[0]};
  border: 1px solid
    ${props =>
    props.isColorValid ? reshader(props.bg).palette[6] : colors.gray[4]};
  border-radius: 3px;
  outline: none;
  color: ${props =>
    props.isColorValid
      ? Color(reshader(props.bg).palette[1]).light()
        ? colors.gray[10]
        : colors.white[0]
      : colors.gray[4]};

  &:focus {
    box-shadow: ${props =>
    props.isColorValid ? reshader(props.bg).palette[1] : colors.blue[1]}
      0 0 0 3px;
  }
`
