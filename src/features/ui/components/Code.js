import styled from 'styled-components'

import { colors } from '..'

export const Code = styled.code`
  background-color: ${colors.white[3]};
  padding: ${props => props.size === 'sm' ? '4px' : '5px'};
  border-radius: 3px;
  color: ${colors.gray[10]};
  font-size: ${props => props.size === 'sm' ? '0.7em' : '1em'}
`
