import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Color from 'color'

import { colors as UIColors } from '..'

const Container = styled.div`
  position: relative;
  display: block;
  width: ${props => props.width || '100%'};
  height: ${props => props.height || '100%'};
`
const Content = styled.div`
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%;
`
const Shades = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`
const NoOutput = Shades.extend`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${UIColors.white[3]};
  width: 100%;
  height: 100%;
  border-radius: 3px;
`
const Text = styled.p`
  margin: 0;
  padding: 0;
  color: ${UIColors.gray[4]};
`
const Shade = styled.div`
  position: relative;
  display: ${props => (props.isVertical ? 'inline-block' : 'block')};
  width: ${props => (props.isVertical ? `${props.size}%` : '100%')};
  height: ${props => (!props.isVertical ? `${props.size}%` : '100%')};
  background-color: ${props => props.color};
`
const ColorName = styled.span`
  display: flex;
  align-items: center;
  padding: 0 10px;
  width: 100%;
  height: 100%;
  font-weight: bold;
  color: ${props => (props.isLight ? UIColors.gray[10] : UIColors.white[0])};
`

export const Shield = ({
  width,
  height,
  colors,
  isVertical,
  shouldIncludeColorNames,
  children
}) => (
  <Container width={width} height={height}>
    <Content>{children}</Content>

    {colors.length === 0 && (
      <NoOutput>
        <Text>An empty array</Text>
      </NoOutput>
    )}

    {colors.length > 0 && (
      <Shades>
        {colors.map((color, index) => (
          <Shade
            key={color + index}
            color={color}
            size={100 / colors.length}
            isVertical={isVertical}
          >
            {shouldIncludeColorNames && (
              <ColorName isLight={Color(color).light()}>{color}</ColorName>
            )}
          </Shade>
        ))}
      </Shades>
    )}
  </Container>
)

Shield.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  colors: PropTypes.array.isRequired,
  isVertical: PropTypes.bool,
  shouldIncludeColorNames: PropTypes.bool,
  children: PropTypes.any
}
