import React from 'react'
import styled from 'styled-components'
import reshader from 'reshader'

import { colors, Shield } from '..'

const Container = styled.a`
  overflow: hidden;
  border-radius: 3px;
  text-decoration: none;
`

const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`

const Text = styled.h1`
  margin: 0;
  padding: 0;
  color: ${colors.gray[10]};
  font-size: 1.5em;
  letter-spacing: 0.1em;
  text-decoration: none;
`

export const Logo = () => (
  <Container href='/'>
    <Shield
      width='200px'
      height='200px'
      colors={
        reshader(colors.white[0], {
          contrastRatio: 0.02,
          shouldIgnoreRepeated: true,
          numberOfVariations: 5
        }).palette.reverse()
      }
      isVertical
    >
      <Content>
        <Text>Reshader</Text>
      </Content>
    </Shield>
  </Container>
)
