import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div``
const Title = styled.h2`
  margin: 0;
  padding: 0;
`

export const Question = ({ title, children }) => (
  <Container>
    <Title>{title}</Title>

    {children}
  </Container>
)

Question.propTypes = {
  title: PropTypes.string,
  children: PropTypes.any
}
