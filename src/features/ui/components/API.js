import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Flex, Box } from 'grid-styled'
import SyntaxHighlighter from 'react-syntax-highlighter/prism'

import { colors, syntax, Permalink, Code, Shield } from '..'

const Container = styled(Flex)`
  padding: 80px 0;
`
const Left = styled(Box)``
const Right = styled(Box)``
const Key = styled.h3`
  margin-top: 0;
`
const Description = styled.p`
  color: ${colors.gray[4]};
  line-height: 2.2em;
`
const Spec = styled.div`
  display: inline-flex;
  justify-content: space-between;
  margin-right: 10px;
  border: 1px solid ${colors.white[4]};
  padding: 15px 10px;
  border-radius: 3px;
  font-size: 0.8em;
  color: ${colors.gray[1]};
`
const Definition = {
  Container: styled.div`
    display: flex;
  `,
  Key: styled.div``,
  Value: styled.div`
    margin-left: 10px;
  `
}
const Demo = styled.div``
const Outputs = styled(Flex)`
  margin-top: 40px;
`
const Output = {
  Container: styled(Box)``,
  Title: styled.p`
    margin: 0;
    padding: 0;
  `,
  Body: styled.div`
    margin-top: 20px;
  `
}

export const API = ({
  argumentName,
  propName,
  specs,
  demoCode,
  color,
  children,
  afterChildren
}) => (
  <Container direction={['column', 'row']}>
    <Left w={[1 / 1, 3 / 8]} mr={100}>
      <Key>
        <Permalink
          id={
            propName ? `API-${argumentName}-${propName}` : `API-${argumentName}`
          }
          href={
            propName
              ? `#API-${argumentName}-${propName}`
              : `#API-${argumentName}`
          }
        >
          #
        </Permalink>{' '}
        {argumentName}
        {propName && `.${propName}`}
      </Key>

      {specs.map(spec => (
        <Spec key={spec.key}>
          <Definition.Container>
            <Definition.Key>{spec.key}</Definition.Key>
            {spec.value && <Definition.Value>{spec.value}</Definition.Value>}
          </Definition.Container>
        </Spec>
      ))}

      <Description>{children}</Description>

      {afterChildren}
    </Left>

    <Right w={[1 / 1, 5 / 8]}>
      <Demo>
        <SyntaxHighlighter
          customStyle={{
            ...syntax,
            margin: 0
          }}
          language='javascript'
        >
          {demoCode}
        </SyntaxHighlighter>

        <Outputs direction={['column', 'row']}>
          <Output.Container w={[1 / 1, 1 / 4]}>
            <Output.Title>
              <Code size='sm'>palette</Code>
            </Output.Title>

            <Output.Body>
              <Shield height='150px' colors={color.palette} />
            </Output.Body>
          </Output.Container>

          <Output.Container w={[1 / 1, 1 / 4]} ml={[0, 20]} mt={[20, 0]}>
            <Output.Title>
              <Code size='sm'>variations.all</Code>
            </Output.Title>

            <Output.Body>
              <Shield height='150px' colors={color.variations.all} />
            </Output.Body>
          </Output.Container>

          <Output.Container w={[1 / 1, 1 / 4]} ml={[0, 20]} mt={[20, 0]}>
            <Output.Title>
              <Code size='sm'>variations.lighter</Code>
            </Output.Title>

            <Output.Body>
              <Shield height='150px' colors={color.variations.lighter} />
            </Output.Body>
          </Output.Container>

          <Output.Container w={[1 / 1, 1 / 4]} ml={[0, 20]} mt={[20, 0]}>
            <Output.Title>
              <Code size='sm'>variations.darker</Code>
            </Output.Title>

            <Output.Body>
              <Shield height='150px' colors={color.variations.darker} />
            </Output.Body>
          </Output.Container>
        </Outputs>
      </Demo>
    </Right>
  </Container>
)

API.propTypes = {
  argumentName: PropTypes.string,
  propName: PropTypes.string,
  specs: PropTypes.arrayOf(PropTypes.object),
  children: PropTypes.array,
  demoCode: PropTypes.string,
  color: PropTypes.object,
  afterChildren: PropTypes.element
}
