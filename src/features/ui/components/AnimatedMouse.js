import React from 'react'
import styled, { keyframes } from 'styled-components'

import { colors } from '..'

const MouseAnimation = keyframes`
  0% {
    opacity: 1;
    top: 29%;
  }

  15% {
    opacity: 1;
    top: 50%;
  }

  50% {
    opacity: 0;
    top: 50%;
  }

  100% {
    opacity: 0;
    top: 29%;
  }
`

const Mouse = styled.div`
  position: relative;
  display: block;
  width: 25px;
  height: 35px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  border: 2px solid ${colors.acqua[2]};
  border-radius: 23px;
`

const Scroll = styled.div`
  position: absolute;
  display: block;
  top: 29%;
  left: 50%;
  width: 5px;
  height: 5px;
  margin: -5px 0 0 -2px;
  background: ${colors.acqua[2]};
  border-radius: 50%;
  -webkit-animation: ani-mouse 2.5s linear infinite;
  -moz-animation: ani-mouse 2.5s linear infinite;
  animation: ani-mouse 2.5s linear infinite;
  animation: ${MouseAnimation} 2.5s linear infinite;
`

export const AnimatedMouse = () => (
  <Mouse>
    <Scroll />
  </Mouse>
)
