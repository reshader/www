import { colors, Anchor } from '..'

export const Permalink = Anchor.extend`
  margin-right: 5px;
  color: ${colors.acqua[3]};
  text-decoration: none;

  &:hover {
    color: ${colors.acqua[6]};
    text-decoration: underline;
  }
`
