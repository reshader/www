import React from 'react'
import Color from 'color'
import reshader from 'reshader'

import { Code, Anchor, API, Tip } from '../../ui'

const RED = '#EB6E6E'
const PURPLE = '#A56EEB'
const YELLOW = '#EBDE6E'
const GREEN = '#30B825'
const WHITE = '#FFFFFF'
const CYAN = '#6ED0EB'
const ORANGE = '#EBA26E'

const code = [
  `
  reshader('${RED}')
  reshader('${Color(RED)
    .rgb()
    .string()}')
  reshader('${Color(RED)
    .hsl()
    .string()}')
  `,

  `
  const {
    palette,
    variations
  } = reshader('${PURPLE}', { numberOfVariations: 10 })
  `,

  `
  const {
    palette,
    variations
  } = reshader('${YELLOW}', { contrastRatio: 0.05 })
  `,

  `
  const {
    palette,
    variations
  } = reshader('${GREEN}', { outputModel: 'rgb' })
  `,

  `
  const {
    palette,
    variations
  } = reshader('${WHITE}', { shouldIgnoreRepeated: true })
  `,

  `
  const {
    palette,
    variations
  } = reshader('${CYAN}', { shouldIgnoreWhites: true })
  `,

  `
  const {
    palette,
    variations
  } = reshader('${ORANGE}', { shouldIgnoreBlacks: true, contrastRatio: 1 })
  `
]

const Container = () => (
  <div>
    <API
      argumentName='color'
      specs={[
        {
          key: 'Type',
          value: <Code>string</Code>
        },
        {
          key: 'Required'
        }
      ]}
      demoCode={code[0]}
      color={reshader(RED)}
    >
      The color you want to extract the shades of. It's a string, but must
      satisfy one of these formats: <Code>hex</Code>, <Code>rgb</Code> or{' '}
      <Code>hsl</Code>.
    </API>

    <API
      argumentName='options'
      propName='numberOfVariations'
      specs={[
        {
          key: 'Type',
          value: <Code>string</Code>
        },
        {
          key: 'Default',
          value: <Code>5</Code>
        }
      ]}
      demoCode={code[1]}
      color={reshader(PURPLE, { numberOfVariations: 10 })}
    >
      The number of variations you want to extract out of the{' '}
      <Anchor href='#API-color'>color</Anchor> you entered. This option will get
      both lighter and darker shades in the same amount, meaning that if you set
      it as <Code>10</Code>, it will get you 10 lighter shades and 10 darker
      shades.
    </API>

    <API
      argumentName='options'
      propName='contrastRatio'
      specs={[
        {
          key: 'Type',
          value: <Code>number</Code>
        },
        {
          key: 'Default',
          value: <Code>0.1</Code>
        }
      ]}
      demoCode={code[2]}
      color={reshader(YELLOW, { contrastRatio: 0.05 })}
      afterChildren={
        <Tip>
          You'll only reach black (<Code>#000000</Code>) when you set this
          property to 1.
        </Tip>
      }
    >
      The contrast ratio of the shades. The lower, the smoother.
    </API>

    <API
      argumentName='options'
      propName='outputModel'
      specs={[
        {
          key: 'Type',
          value: <Code>string</Code>
        },
        {
          key: 'Default',
          value: <Code>hex</Code>
        }
      ]}
      demoCode={code[3]}
      color={reshader(GREEN, { outputModel: 'rgb' })}
    >
      The color model reshader gonna output regardless of the color model of the
      input. That being said, if you entered a hex code, it'll output the model
      you set through this option. Please, use only one of the{' '}
      <Anchor href='#accepted-output-models'>accepted output models</Anchor>.
    </API>

    <API
      argumentName='options'
      propName='shouldIgnoreRepeated'
      specs={[
        {
          key: 'Type',
          value: <Code>boolean</Code>
        },
        {
          key: 'Default',
          value: <Code>false</Code>
        }
      ]}
      demoCode={code[4]}
      color={reshader(WHITE, { shouldIgnoreRepeated: true })}
    >
      When you get the shades of <Code>#FFFFFF</Code>, the lighter shades of it
      will all be <Code>#FFFFFF</Code> because, well, there's no lighter color
      than white. By setting this option to <Code>true</Code>, your output won't
      retrieve the repeated variations, except the <Code>#FFFFFF</Code> itself.
    </API>

    <API
      argumentName='options'
      propName='shouldIgnoreWhites'
      specs={[
        {
          key: 'Type',
          value: <Code>boolean</Code>
        },
        {
          key: 'Default',
          value: <Code>false</Code>
        }
      ]}
      demoCode={code[5]}
      color={reshader(CYAN, { shouldIgnoreWhites: true })}
    >
      Set this option to <Code>true</Code> when you don't want whites in your
      color palette or shade variations.
    </API>

    <API
      argumentName='options'
      propName='shouldIgnoreBlacks'
      specs={[
        {
          key: 'Type',
          value: <Code>boolean</Code>
        },
        {
          key: 'Default',
          value: <Code>false</Code>
        }
      ]}
      demoCode={code[6]}
      color={reshader(ORANGE, { shouldIgnoreBlacks: true, contrastRatio: 1 })}
    >
      Set this option to <Code>true</Code> when you don't want blacks in your
      color palette or shade variations.
    </API>
  </div>
)

export default Container
