import React from 'react'
import PropTypes from 'prop-types'
import { compose, withState, withHandlers } from 'recompose'
import styled from 'styled-components'
import SyntaxHighlighter from 'react-syntax-highlighter/prism'
import reshader from 'reshader'
import { Flex, Box } from 'grid-styled'
import 'normalize.css/normalize.css'

import {
  colors,
  syntax,
  Anchor,
  Permalink,
  Shield,
  Logo,
  Code,
  ColorfulInput,
  AnimatedMouse,
  Tip,
  Question
} from './features/ui'

import API from './features/api/components/Container'

import Guilherme from './images/author.jpg'

const CenteredContainer = styled.div`
  max-width: 640px;
  padding: 0 40px;
  margin-left: auto;
  margin-right: auto;
`

const HugeContainer = CenteredContainer.extend`
  max-width: 1280px;
`

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`

const Introduction = styled.div`
  position: relative;
  margin-bottom: 200px;
  width: 100%;
  height: 90vh;
  text-align: center;
  background-color: ${colors.acqua[6]};

  @media (min-width: 720px) {
    height: 100vh;
  }
`

const IntroductionContent = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
`

const IntroductionPackage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  flex: 1;
`

const Badges = styled.div`
  margin-top: 20px;
`

const Badge = styled.div`
  display: inline-block;

  &:not(:first-child) {
    margin-left: 10px;
  }
`

const Presentation = styled.div`
  margin-top: 10px;
`

const Lead = styled.p`
  padding: 0 40px;
  font-size: 1.6em;
  color: ${colors.white[0]};
  font-weight: 600;

  @media (min-width: 720px) {
    max-width: 600px;
    font-size: 2em;
  }
`

const Advice = styled.div`
  justify-content: center;
  align-items: center;
  display: none;
  width: 100%;
  color: ${colors.acqua[1]};

  @media (min-width: 720px) {
    display: flex;
    margin-bottom: 100px;
  }
`

const AdviceText = styled.small`
  margin-left: 10px;
`

const Button = styled.a`
  padding: 10px 15px;
  border-radius: 20px;
  text-decoration: none;
  font-weight: 600;
  color: ${colors.gray[5]};
  background-color: ${colors.white[0]};
`

const Body = styled.main`
  position: relative;
`

const Section = styled.section`
  padding: 200px 0;
  width: 100%;
`

const Explanation = styled.div`
  padding: 20px 0;
`

const Title = styled.h1`
  margin: 0;
  padding: 0;
  font-weight: 200;
  color: ${colors.gray[10]};
`

const Subtitle = styled.h2`
  margin: 0;
  padding: 10px 0;
  color: ${colors.gray[10]};
`

const P = styled.p`
  padding: 10px 0;
  margin: 0;
  color: ${colors.gray[8]};
  font-size: 1.1em;
  line-height: 1.8em;
`

const Ul = styled.ul``

const Li = styled.li``

const QuestionContainer = styled.div`
  margin: 40px 0;
`

const AuthorContainer = styled(Flex)`
  font-size: 1.2em;
`

const AuthorDescription = styled.p`
  margin: 10px 0 0;
  padding: 0;
  color: ${colors.gray[6]};
  font-size: 0.8em;
`

const Avatar = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 3px;
`

const Muted = styled(P)`
  color: ${colors.white[6]};
  font-size: 0.9em;
`

const Center = styled.div`
  padding-bottom: 200px;
  text-align: center;
`

const Warning = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 2;
  padding: 20px;
  width: 100%;
  box-sizing: border-box;
  background-color: ${colors.gray[10]};
  color: ${colors.white[0]};
  text-align: center;
`

const codeStrings = [
  ({ color }) => `
  import reshader from 'reshader'

  const { palette } = reshader('${color}')
  `,

  `
  // ./UI/colors.js

  export const colors = {
    gray: {
      0: '#FAFAFA',
      1: '#F2F2F2',
      2: '#DDDDDD'
    },

    red: {
      0: '#FFBBBB',
      1: '#FF8080',
      2: '#FF5050'
    },

    blue: {
      0: '#B6CBFF',
      1: '#8AABFF',
      2: '#326CFF'
    }
  }
  `,

  `
  // MyComponent.js

  import React from 'react'
  import styled from 'styled-components'
  import { colors } from './UI'

  const Container = styled.div\`
    background-color: \${colors.gray[0]};
  \`

  const MyComponent = ({ children }) => <Container>{children}</Container>

  export default MyComponent
  `,

  `
  import reshader from 'reshader'

  export const colors = {
    gray: reshader('#DDDDDD').palette,
    red: reshader('#FF5050').palette,
    blue: reshader('#326CFF').palette
  }
  `
]

const App = ({ color, onChangeInput, isColorValid }) => {
  return (
    <div>
      <Warning>
        Thanks to the feedback of several people, a detailed walkthrough of the
        Reshader usage was published.{' '}
        <Anchor
          href="https://medium.com/@chiefgui/managing-scalable-consistent-colors-in-your-css-in-js-app-with-reshader-ab29f03f1a35"
          target="_blank"
          rel="noreferrer noopener"
        >
          Check it out!
        </Anchor>
      </Warning>

      <Introduction>
        <Overlay>
          <Shield
            width="100%"
            height="100%"
            colors={reshader(colors.acqua[5], {
              numberOfVariations: 5,
              contrastRatio: 0.05,
              shouldIgnoreRepeated: true
            }).palette.reverse()}
            isVertical
          />
        </Overlay>
        <IntroductionContent>
          <IntroductionPackage>
            <Logo />
            <Badges>
              <Badge>
                <a
                  href="https://npmjs.com/package/reshader"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://img.shields.io/npm/v/reshader.svg"
                    alt="version shield"
                  />
                </a>
              </Badge>

              <Badge>
                <a
                  href="https://travis-ci.org/chiefGui/reshader"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://img.shields.io/travis/chiefGui/reshader.svg"
                    alt="ci shield"
                  />
                </a>
              </Badge>

              <Badge>
                <img
                  src="https://img.shields.io/npm/l/reshader.svg"
                  alt="license shield"
                />
              </Badge>

              <Badge>
                <a
                  href="https://github.com/chiefGui/reshader"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://img.shields.io/github/stars/chiefGui/reshader.svg?style=flat&label=Stars"
                    alt="stars shield"
                  />
                </a>
              </Badge>
            </Badges>
            <Presentation>
              <Lead>
                A micro, highly-customisable JavaScript library to get the
                shades of colors
              </Lead>
            </Presentation>

            <Flex mt={20}>
              <Box mr={10}>
                <Button href="#getting-started">Quick start</Button>
              </Box>

              <Box>
                <Button
                  href="https://github.com/chiefGui/reshader"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-github" /> GitHub &raquo;
                </Button>
              </Box>
            </Flex>
          </IntroductionPackage>

          <Advice>
            <AnimatedMouse />

            <AdviceText>Scroll down to see more</AdviceText>
          </Advice>
        </IntroductionContent>
      </Introduction>

      <HugeContainer>
        <Body>
          <Section>
            <Title>
              <Permalink id="getting-started" href="#getting-started">
                #
              </Permalink>{' '}
              Getting started
            </Title>

            <P>It's very simple to comence:</P>

            <P>
              <Code>yarn add reshader</Code> or{' '}
              <Code>npm install reshader</Code>
            </P>

            <P>
              <strong>Note:</strong> In case you are using RequireJS or want
              reshader on your <Code>window</Code>, please refer to{' '}
              <Anchor href="https://github.com/chiefGui/reshader/blob/master/dist/reshader.js">
                the distribution file
              </Anchor>
              .
            </P>
          </Section>

          <Section>
            <Title>
              <Permalink id="usage" href="#usage">
                #
              </Permalink>{' '}
              Usage
            </Title>

            <Flex direction={['column', 'row']}>
              <Box w={[1 / 1, 3 / 8]} mr={[0, 100]}>
                <P>
                  You can simply call <Code>reshader</Code> passing a{' '}
                  <Code>color</Code> in the first parameter.
                </P>

                <P>
                  It returns an object with two properties—
                  <Anchor href="#understanding-palette-and-variations">
                    you will learn more further
                  </Anchor>
                  —with the variations of the given color.
                </P>

                <P>
                  Use and abuse of the <Anchor href="#API">API</Anchor> to
                  customise the output as much as you want or need.
                </P>

                <Tip>
                  Reshader will always sort the colors it returns from the
                  lightest shade to the darkest.
                </Tip>
              </Box>
              <Box py={20} w={[1 / 1, 5 / 8]}>
                <ColorfulInput
                  style={{ marginBottom: 40 }}
                  bg={color}
                  isColorValid={isColorValid}
                  type="text"
                  value={color}
                  onChange={onChangeInput}
                />

                <SyntaxHighlighter
                  customStyle={{
                    ...syntax,
                    margin: '0 0 40px'
                  }}
                  language="javascript"
                >
                  {codeStrings[0]({ color })}
                </SyntaxHighlighter>

                <Shield
                  width="100%"
                  height="500px"
                  colors={isColorValid ? reshader(color).palette : []}
                  shouldIncludeColorNames
                />
              </Box>
            </Flex>
          </Section>

          <Section>
            <Title>
              <Permalink id="API" href="#API">
                #
              </Permalink>{' '}
              API
            </Title>

            <P>
              <Code>reshader</Code> takes two parameters: <Code>color</Code> and{' '}
              <Code>options</Code> respectively.
            </P>

            <API />
          </Section>

          <Section>
            <Title>
              <Permalink
                id="understanding-palette-and-variations"
                href="#understanding-palette-and-variations"
              >
                #
              </Permalink>{' '}
              Understanding <Code>palette</Code> and <Code>variations</Code>
            </Title>

            <P>
              <Code>reshader('#FF0000')</Code> returns an object containing the{' '}
              <Code>palette</Code> and <Code>variations</Code> properties. Let's
              take a look on how they work.
            </P>

            <Explanation>
              <Subtitle>
                palette <Code>array</Code>
              </Subtitle>
              <P>
                An array containing all the shades <strong>plus</strong> the
                color you passed in the first argument. That being said,{' '}
                <Code>reshader('#FF0000').palette</Code> would return an array
                with 11 indices. 5 lighter shades, 5 darker shades and{' '}
                <Code>#FF0000</Code>. It'll always be positioned right in the
                middle of the array—in our case, you'll be able to find it
                accessing the index <Code>5</Code>.
              </P>
            </Explanation>

            <Explanation>
              <Subtitle>
                variations <Code>object</Code>
              </Subtitle>

              <Ul>
                <Li>
                  <P>
                    <strong>.all</strong> <Code>array</Code>
                  </P>

                  <P>
                    Returns <strong>only</strong> the variations of a given
                    color, without the given color itself. For knowledge's
                    sakes, The <Code>palette</Code> array contains 11 elements
                    whilst <Code>variations</Code> will expose only 10 because
                    it discards the given color. Basically, it's a merge of{' '}
                    <Code>variations.lighter</Code> and{' '}
                    <Code>variations.darker</Code>.
                  </P>

                  <Tip>
                    There will be a jump between the colors in the middle of the
                    this array because the given color will always be the most
                    neutral shade and is unavailable here.
                  </Tip>
                </Li>
                <Li>
                  <P>
                    <strong>.lighter</strong> <Code>array</Code>
                  </P>

                  <P>
                    Returns <strong>only</strong> the lighter shades of the
                    given color, including whites. If the{' '}
                    <Code>numberOfVariations</Code> is <Code>10</Code>, this
                    array will have 10 elements and it{' '}
                    <strong>doesn't include the given color.</strong>
                  </P>
                </Li>
                <Li>
                  <P>
                    <strong>.darker</strong> <Code>array</Code>
                  </P>

                  <P>
                    Returns <strong>only</strong> the darker shades of the given
                    color, including blacks. If the{' '}
                    <Code>numberOfVariations</Code> is <Code>10</Code>, this
                    array will have 10 elements and it{' '}
                    <strong>doesn't include the given color.</strong>
                  </P>
                </Li>
              </Ul>
            </Explanation>

            <Tip title="To emphasize">
              Regardless of what array you're working with (palette,
              variations.all, etc.), Reshader will always sort them from the
              lightest shade to the darkest, being the value on the index 0 the
              lightest and the value on the last index the darkest.
            </Tip>
          </Section>

          <Section>
            <Title>
              <Permalink
                id="accepted-output-models"
                href="#accepted-output-models"
              >
                #
              </Permalink>{' '}
              Accepted output models
            </Title>

            <P>
              The accepted models you can input are slightly different from the
              ones reshader is capable of output. It only accepts three entrance
              color models: <Code>hex</Code>, <Code>rgb</Code> and{' '}
              <Code>hsl</Code>, whilst it is capable of output <Code>hex</Code>{' '}
              (default), <Code>rgb</Code>, <Code>hsl</Code>, <Code>hsv</Code>,{' '}
              <Code>cmyk</Code> and <Code>ansi256</Code>. This is because it
              follows the API provided by its core,{' '}
              <Anchor href="https://github.com/Qix-/color" target="_blank">
                Qix's color
              </Anchor>
              .
            </P>
          </Section>

          <Section>
            <Title>
              <Permalink id="motivation" href="#motivation">
                #
              </Permalink>{' '}
              Motivation
            </Title>

            <P>
              It started when{' '}
              <Anchor
                href="https://github.com/chiefGui"
                target="_blank"
                rel="noopener noreferrer"
              >
                I
              </Anchor>{' '}
              needed a way to have consistent colors in my css-in-js apps.
              Unfortunately, picking colors by the eye was not getting me the
              best results, specially on the long term, and it was consuming
              unnecessary amounts of time to get the shades I wanted to. Prior
              to Reshader, I used to have a <Code>colors.js</Code> file and it
              used to look like this:
            </P>

            <SyntaxHighlighter customStyle={syntax} language="javascript">
              {codeStrings[1]}
            </SyntaxHighlighter>

            <SyntaxHighlighter customStyle={syntax} language="jsx">
              {codeStrings[2]}
            </SyntaxHighlighter>

            <P>
              As you can see, all the colors were handpicked, manually mapped
              and prone to inconsistencies. But I wanted something
              programatically, consistent and simple. Then I came up with
              Reshader and my new <Code>colors.js</Code> looks just like this:
            </P>

            <SyntaxHighlighter customStyle={syntax} language="javascript">
              {codeStrings[3]}
            </SyntaxHighlighter>

            <P>
              Boom! It's working with the same implementation as before, where{' '}
              <Code>gray[0]</Code> is the lightest shade and{' '}
              <Code>gray[gray.length - 1]</Code> is the darkest.
            </P>

            <Muted>
              In case you want to deep dive into the motivation and usage,{' '}
              <Anchor
                href="https://medium.com/@chiefgui/managing-scalable-consistent-colors-in-your-css-in-js-app-with-reshader-ab29f03f1a35"
                target="_blank"
                rel="noopener noreferrer"
              >
                please read our walkthrough article
              </Anchor>
              .
            </Muted>
          </Section>

          <Section>
            <Title>
              <Permalink id="faq" href="#faq">
                #
              </Permalink>{' '}
              Frequently Asked Questions
            </Title>

            <QuestionContainer>
              <Question title="How are the mathematics behind the colors being done?">
                <P>
                  Please, refer to{' '}
                  <Anchor
                    href="https://github.com/Qix-/color/blob/master/index.js"
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    Qix-'s Color source file
                  </Anchor>
                  .
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="Why would I use Reshader?">
                <P>
                  Shallowly saying, if you want or need to generate shades of
                  colors programatically.
                </P>

                <P>
                  Also,{' '}
                  <Anchor
                    href="https://medium.com/@chiefgui/managing-scalable-consistent-colors-in-your-css-in-js-app-with-reshader-ab29f03f1a35"
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    Managing scalable, consistent colors in your CSS-in-JS app
                    with Reshader
                  </Anchor>{' '}
                  might be useful for you to comprehend everything in depth.
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="Why would I use Reshader rather than ___?">
                <P>
                  Good question though the answer is very trivial: use your
                  reality to respond yourself. Maybe it would be useful, maybe
                  not. If you feel that another solution helps you better, go
                  for it!
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="How can developers and designers work together with Reshader?">
                <P>
                  At first, Reshader was designed to help <strong>only</strong>{' '}
                  developers, but after some feedbacks from different
                  perspectives, it must be said that this answer is yet to come.
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="Isn't Reshader an overkill?">
                <P>
                  Maybe, yet it's very practical. From the distant outlook, it
                  seems way too much to handle such a simple thing, but once you
                  start using it, you're gonna feel the convenience. Not worring
                  about shades but only the main colors of your app is beautiful
                  and gonna boost your maintainance reability as well as your
                  productivity.
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="Isn't performance an issue?">
                <P>
                  It depends. It may be an issue if you call{' '}
                  <Code>reshader</Code> multiple times in runtime, but if you
                  glue it to compilation time you're probably fine.
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="Why the website of this open source library is under a .com domain?">
                <P>
                  Because .com is cheaper than .io yet widely known and easy to
                  remember. That's it.
                </P>
              </Question>
            </QuestionContainer>

            <QuestionContainer>
              <Question title="Where should I submit my questions, feedbacks, issues, ideas, etc?">
                <P>
                  Make{' '}
                  <Anchor
                    href="https://github.com/chiefGui/reshader/issues"
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    our repository
                  </Anchor>{' '}
                  yours.
                </P>
              </Question>
            </QuestionContainer>
          </Section>

          <Section>
            <Title>
              <Permalink id="author" href="#author">
                #
              </Permalink>{' '}
              Author
            </Title>

            <AuthorContainer
              mt={20}
              direction={['column', 'row']}
              align="center"
            >
              <Box mr={[0, 20]} w={[1 / 1, 1 / 6]}>
                <Avatar
                  src={Guilherme}
                  alt="A photo of Guilherme Oderdenge, the author of Reshader."
                />
              </Box>

              <Box py={[20, 0]} w={[1 / 1, 5 / 6]}>
                <Anchor
                  href="https://github.com/chiefGui"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  Guilherme "chiefGui" Oderdenge
                </Anchor>

                <AuthorDescription>
                  A Brazilian JavaScript developer.
                </AuthorDescription>
              </Box>
            </AuthorContainer>

            <Muted>
              Also, a special thanks to{' '}
              <Anchor
                href="https://github.com/Qix-"
                target="_blank"
                rel="noreferrer noopener"
              >
                Josh Junon
              </Anchor>
              , the author of the amazing{' '}
              <Anchor href="https://github.com/Qix-/color">Color</Anchor>—the
              library dealing with the magic Reshader uses to create the shades.
            </Muted>
          </Section>
        </Body>

        <Center>
          <Anchor href="#" isMuted>
            Back to the top ↑
          </Anchor>
        </Center>
      </HugeContainer>

      <Shield
        width="100%"
        height="100px"
        colors={
          reshader(colors.acqua[5], {
            numberOfVariations: 1,
            shouldIgnoreRepeated: true
          }).palette
        }
      />
    </div>
  )
}

App.propTypes = {
  color: PropTypes.string,
  onChangeInput: PropTypes.func,
  isColorValid: PropTypes.bool
}

export default compose(
  withState('color', 'setColor', colors.acqua[5]),
  withState('isColorValid', 'setIsColorValid', true),
  withHandlers({
    onChangeInput: ({ setColor, isColorValid, setIsColorValid }) => event => {
      setColor(event.target.value)

      try {
        reshader(event.target.value)

        if (!isColorValid) {
          setIsColorValid(true)
        }
      } catch (_) {
        if (isColorValid) {
          setIsColorValid(false)
        }
      }
    }
  })
)(App)
